from ROOT import *
import numpy as np
import sys
import os
from array import array
from Particle_data import labels,lhcb_info

subdet = lhcb_info().subdetectors_Zpos
label = labels().label_title
inputDir="/afs/cern.ch/work/d/dmendoza/public/summer_project/diracSandbox/Analisis/tuples"

#Funtion that obtains the number of track type or TT,DD,LL, or rest for a decay channel for "true" data
def track_num(trackOpt,treeList,decType,massList,lifetimeList):

    if trackOpt == "vertex": # in ["TT","LL","DD","rest"]:
        file_vertex = open("tracks_vertex.txt","w")
        for i,tree in enumerate(treeList):
            val = array('d',[0.])
            tree.SetBranchAddress("Higgs0_TRUEENDVERTEX_Z",val)
            LL =   0
            DD =   0
            TT =   0
            rest = 0
            
            for j in range(tree.GetEntries()):
                tree.GetEntry(j)
                if (val[0] < subdet[1][1]):
                    LL+=1                                        
                else:                                                                   
                    if (val[0] < subdet[2][1]): 
                        DD+=1                                    
                    else:                                                               
                        if (val[0] < subdet[3][1]):  
                            TT+=1                                
                        else:                                                           
                            rest+=1
            file_vertex.write('"'+decType[i]+'":{"mass":"'+massList[i]+'","lifetime":"'+lifetimeList[i]+'","LL":"'+str(int(LL))+'","DD":"'+str(int(DD))+'","TT":"'+str(int(TT))+'","rest":"'+str(int(rest))+'"},\n')                                             
#"12113082":{"mass":"500","lifetime":"100","LL":"2470","DD":"991","TT":"1321","rest":"607"},
        file_vertex.close() 
            
        
        
    # elif TrackOpt == "track":  #in ["LTrack","DTrack","TTrack","UTrack","VTrack"]:
    #     file_tracks = open("trecks_types.txt","w")
    #     for i,tree in treeList:
    #         for j,track in ["Long","Downstream","T","Velo","Upstream"]:
    #             Long = array('d',[0.])
    #             Down = array('d',[0.])
    #         T = array('d',[0.])
    #         Velo = array('d',[0.])
    #         Up = array('d',[0.])

    #         tree.SetBranchAddress("Higgs0_TRUEENDVERTEXZ",val)
    else: return

if True:
    decNumbers = ["220","235","214","800","250"]
    lifetimes = []
    masses = ["220","235","214","800","250"]

    lifetime = 1.0000e-10#,2.5000e-10,5.0000e-10,7.5000e-10,1.0000e-9,1.2500e-9,1.5000e-9,1.7500e-9,2.000e-9]
    
    
   
   
        
   
    for i in range(5):
        lifetimes.append(str(int(lifetime*1.e12)))
    #     mass = 0.5*(1+i)
    #     for j in range(len(lifetime)):
    #         decNumbers.append(str(int(mass*1000+j)))
    #         lifetimes.append(str(int(lifetime[j]*1.e12)))
    #         masses.append(str(mass))


    allFiles = [TFile.Open(inputDir+"/DVntuple_"+decNumber+".root") for decNumber in decNumbers]
    allTrees = [f.Get("MCDecayTreeTuple/MCDecayTree") for f in allFiles]
    
    track_num("vertex",allTrees,decNumbers,masses,lifetimes)
