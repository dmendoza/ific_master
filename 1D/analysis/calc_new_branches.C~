#include <iostream>
#include <fstream>
#include <vector>
#include <iterator>
#include <string>
#include <algorithm>

#include "TH1.h"
#include "TH2.h"
#include "TFile.h"
#include "TBranch.h"
#include "TTree.h"
#include "TCanvas.h"
#include "TSystem.h"

#include "TF1.h"
#include "TGraph.h"
#include "TGraphQQ.h"
#include "TCanvas.h"
#include "TStyle.h"
#include "TMath.h"
#include "TLorentzVector.h"

#include <Math/GenVector/Boost.h>

#include "Math/Vector3D.h"
#include "Math/Vector4D.h"

using namespace ROOT::Math;
using namespace std;
//This function is only used for the decay H10->mu+mu-
void calc_helicity_f(TString file_name, TString tree_name)
{
  TString file = file_name;
  TString tree = tree_name;

  TFile * f = TFile::Open(file, "update");
  TTree * T = (TTree*)f->Get(tree);
  
  double Higgs0_dec_hel;
  TBranch * helicity = T->Branch("Higgs0_decay_helicity", &Higgs0_dec_hel, "Higgs0_dec_hel/D");
  
  std::map<TString,ROOT::Math::LorentzVector<ROOT::Math::PxPyPzE4D<double>>> P_labframe;
  std::map<TString,double[4]> values;

  for (TString pt : {"B0", "Higgs0", "muplus"})
    {
      P_labframe[pt] = ROOT::Math::LorentzVector<ROOT::Math::PxPyPzE4D<double>>();
      T->SetBranchAddress(pt+"_TRUEP_X", &values[pt][0]);
      T->SetBranchAddress(pt+"_TRUEP_Y", &values[pt][1]);
      T->SetBranchAddress(pt+"_TRUEP_Z", &values[pt][2]);
      T->SetBranchAddress(pt+"_TRUEP_E", &values[pt][3]);
      
    }

  ROOT::Math::Boost Bplusbt(0.,0.,0.);// = new ROOT::Math::Boost();
  ROOT::Math::Boost Higgsbt(0.,0.,0.);// = new ROOT::Math::Boost();
  
  ROOT::Math::DisplacementVector3D<ROOT::Math::Cartesian3D<double>> v1;
  ROOT::Math::DisplacementVector3D<ROOT::Math::Cartesian3D<double>> v2;
  
  double b1,b2,b3;
  int entries = T->GetEntries();
  for(int i = 0;i<entries;i++)
  {
    T->GetEntry(i);
    for(TString pt: {"B0","Higgs0","muplus"}) 
      {
	P_labframe[pt].SetPxPyPzE(values[pt][0],values[pt][1],values[pt][2],values[pt][3]);
      }
    
    Bplusbt.SetComponents(P_labframe["B0" ].BoostToCM());
    Higgsbt.SetComponents(P_labframe["Higgs0"].BoostToCM());
    
    Bplusbt.GetComponents(b1,b2,b3);
    cout << b1 << b2 << b3;
    v1 = Bplusbt(P_labframe["Higgs0"]).Vect();
    v2 = Higgsbt(P_labframe["muplus"]).Vect();
    Higgs0_dec_hel =TMath::ACos(v1.Dot(v2)/(TMath::Sqrt(v1.mag2()*v2.mag2())));
    
    helicity->Fill();
    
    }
  
  T->Print();
  f->Write();
  f->Close();

}

void calc_decaylength_f(TString file_name, TString tree_name, TString particle_name)
{
  TString file = file_name;
  TString tree = tree_name;
  TString particle = particle_name;

  //Names for the branches of the origin vertex
  TString br_OVx= particle+"_TRUEORIGINVERTEX_X";
  TString br_OVy= particle+"_TRUEORIGINVERTEX_Y";
  TString br_OVz= particle+"_TRUEORIGINVERTEX_Z";

  //Names for the branches of the end vertex
  TString br_EVx= particle+"_TRUEENDVERTEX_X";
  TString br_EVy= particle+"_TRUEENDVERTEX_Y";
  TString br_EVz= particle+"_TRUEENDVERTEX_Z";

  TFile * f = TFile::Open(file, "update");
  TTree * T = (TTree*)f->Get(tree);
  
  double OVx, OVy, OVz, EVx, EVy, EVz, dist;
 
  //if(     particle == "Bplus" ) double Bplus_decaylenght;
  //else if(particle == "Higgs0") double B 
  
  TString branch_name = particle+"_decaylength";

  TBranch * decaylength = T->Branch(branch_name, &dist, branch_name+"/D");
  
  T->SetBranchAddress(br_OVx, &OVx);
  T->SetBranchAddress(br_OVy, &OVy);
  T->SetBranchAddress(br_OVz, &OVz);

  T->SetBranchAddress(br_EVx, &EVx);
  T->SetBranchAddress(br_EVy, &EVy);
  T->SetBranchAddress(br_EVz, &EVz);
  
  int entries = T->GetEntries();
  for(int i = 0;i<entries;i++)
  {
    T->GetEntry(i);
    
    dist = TMath::Sqrt(pow((EVx-OVx),2)+pow((EVy-OVy),2)+pow((EVz-OVz),2));
    decaylength->Fill();
  }
  
  T->Print();
  f->Write();
  f->Close();

}
  

void calc_angle_mus_f(TString file_name, TString tree_name)
{
  TString file = file_name;
  TString tree = tree_name;
  TString particle1 = "muplus";
  TString particle2 = "muminus";
  TString br_px1= particle1+"_TRUEP_X";
  TString br_py1= particle1+"_TRUEP_Y";
  TString br_pz1= particle1+"_TRUEP_Z";

  TString br_px2= particle2+"_TRUEP_X";
  TString br_py2= particle2+"_TRUEP_Y";
  TString br_pz2= particle2+"_TRUEP_Z";

  TFile * f = TFile::Open(file, "update");
  TTree * T = (TTree*)f->Get(tree);
  
  //Defining the momentum variables, 1 for mu+ and 2 for mu-
  double px1, py1, pz1, px2, py2, pz2, angle;
  
  TString branch_name = "mumu_Theta";

  TBranch * angle_br = T->Branch(branch_name, &angle, branch_name+"/D");
  
  //Seting the address of the branches I want to read
  T->SetBranchAddress(br_px1, &px1);
  T->SetBranchAddress(br_py1, &py1);
  T->SetBranchAddress(br_pz1, &pz1);
  T->SetBranchAddress(br_px2, &px2);
  T->SetBranchAddress(br_py2, &py2);
  T->SetBranchAddress(br_pz2, &pz2);
  
  int entries = T->GetEntries();
  for(int i = 0;i<entries;i++)
  {
    T->GetEntry(i);
    
    angle =TMath::ACos((px1*px2+py1*py2+pz1*pz2)/TMath::Sqrt((px1*px1+py1*py1+pz1*pz1)*(px2*px2+py2*py2+pz2*pz2)));
    angle_br->Fill();
  }
  
  T->Print();
  f->Write();
  f->Close();

}
void calc_total_momentum(TString file_name, TString tree_name, TString particle_name)
{
  TString br_px= particle_name+"_TRUEP_X";
  TString br_py= particle_name+"_TRUEP_Y";
  TString br_pz= particle_name+"_TRUEP_Z";
  
  TFile * f = TFile::Open(file_name, "update");
  TTree * T = (TTree*)f->Get(tree_name);
  
  double px, py, pz, p_tot;
  
  TString branch_name = particle_name+"_TRUEP_TOT";

  TBranch * tot_momentum = T->Branch(branch_name, &p_tot, branch_name+"/D");
  
  T->SetBranchAddress(br_px, &px);
  T->SetBranchAddress(br_py, &py);
  T->SetBranchAddress(br_pz, &pz);
  
  int entries = T->GetEntries();
  for(int i = 0;i<entries;i++)
  {
    T->GetEntry(i);
    
    p_tot = TMath::Sqrt(px*px+py*py+pz*pz);
    tot_momentum->Fill();
  }
  
  T->Print();
  f->Write();
  f->Close();


}
void calc_invariant_mass_f(TString file_name, TString tree_name, TString particle_name)
{
  TString file = file_name;
  TString tree = tree_name;
  TString particle = particle_name;
  TString br_E = particle+"_TRUEP_E";
  TString br_px= particle+"_TRUEP_X";
  TString br_py= particle+"_TRUEP_Y";
  TString br_pz= particle+"_TRUEP_Z";

  TFile * f = TFile::Open(file, "update");
  TTree * T = (TTree*)f->Get(tree);
  
  double E, px, py, pz, M;
  
  TString branch_name = particle+"_INVARIANT_M";

  TBranch * mass = T->Branch(branch_name, &M, branch_name+"/D");
  
  T->SetBranchAddress(br_E, &E);
  T->SetBranchAddress(br_px, &px);
  T->SetBranchAddress(br_py, &py);
  T->SetBranchAddress(br_pz, &pz);
  
  int entries = T->GetEntries();
  for(int i = 0;i<entries;i++)
  {
    T->GetEntry(i);
    
    M = TMath::Sqrt(E*E-px*px-py*py-pz*pz);
    mass->Fill();
  }
  
  T->Print();
  f->Write();
  f->Close();

}
 

  
int calc_new_branches()
{
  
  TString treeName = "MCDecayTreeTuple/MCDecayTree";

  for (TString decType: {"12113082","12113083","12113085","12113086","12113088","12113089","12113091","12113092","12113094","12113096","12113097","12113098","12113099"})
    {
      TString fileName = "./tuples/DVntuple_"+decType+".root";
      for (TString pt: {"Bplus","Higgs0","muminus","muplus","Kplus"})
	{
	  
	  calc_invariant_mass_f(fileName,treeName,pt);
	  calc_total_momentum(fileName,treeName,pt);
	  if (pt == "Bplus" || pt == "Higgs0") calc_decaylength_f(fileName,treeName,pt);
	}
      calc_helicity_f(fileName,treeName);
      calc_angle_mus_f(fileName,treeName);
    }
    
  return 0;


}
