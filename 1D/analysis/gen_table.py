#Python script ot read the information in a txt and produce a table

from pylatex import Document, LongTable, MultiColumn
from Particle_data import particles_info, lhcb_info, labels

dkbs = particles_info().dark_boson

def read_from_file(filename):
    with open(filename) as f:
        lines = f.next()
    lines = lines.rstrip('\n').split(" ")
    f.close()
    return lines

def obtain_proportions(arr):
    v1 = float(arr[1])
    v2 = float(arr[2])
    v3 = float(arr[3])
    v4 = float(arr[4])
    tot = v1+v2+v3+v4
    return ["M = "+darkboson[arr[0]][0]+" MeV, #tau = "+darkboson[arr[0]][1]+ " ps",str(round(v1/tot*100,2))+" #pm "+str(round(100/tot*sqrt(v1),2)),str(round(v2/tot*100,2))+" #pm "+str(round(100/tot*sqrt(v2),2)),str(round(v3/tot*100,2))+" #pm "+str(round(100/tot*sqrt(v3),2)),str(round(v4/tot*100,2))+" #pm "+str(round(100/tot*sqrt(v4),2))]


def genenerate_longtabu():
    geometry_options = {
        "margin": "2.54cm",
        "includeheadfoot": True
    }
    doc = Document(page_numbers=True, geometry_options=geometry_options)

    # Generate data table
    with doc.create(LongTable("l l l l l")) as data_table:
            data_table.add_hline()
            data_table.add_row(["Particle properties", "VELO", "VELO-UT", "UT-SCIFI", "SCIFI-FURTHER"])
            data_table.add_hline()
            data_table.end_table_header()
            data_table.add_hline()
            data_table.add_row((MultiColumn(5, align='r',
                                data='Continued on Next Page'),))
            data_table.add_hline()
            data_table.end_table_footer()
            data_table.add_hline()
            data_table.add_row((MultiColumn(5, align='r',
                                data='Not Continued on Next Page'),))
            data_table.add_hline()
            data_table.end_table_last_footer()
            
            f = open("test.txt","r")
          
            lines = f.readlines()
            for line in lines:
                
                row = line.rstrip('\n').split(" ")
                new_row = obtain_proportions(row)
                data_table.add_row(new_row)

    doc.generate_pdf("longtable", clean_tex=False)

genenerate_longtabu()

