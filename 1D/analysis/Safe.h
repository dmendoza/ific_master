
namespace Safe{
  
  TFile* Open(TString fileName)
  {
    TFile *f = TFile::Open(fileName, "READ");
    if (!f)
      {
        std::cout << "ERROR: Did not find " << fileName << std::endl;
        std::exit(1);//I never learned how to manage exceptions but it would be cleaner
      }
    return f;
  }


};
