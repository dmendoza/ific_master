import numpy as np

class particles_info():
    
    def __init__(self):
        #Information for dark bosson. Each key is a channel, with first argument: mass in MeV/c^2 and second: lifetime in ps (10^-12 s)
        self.dark_boson = {"12113082":{"mass":"500","lifetime":"100","LL":"2470","DD":"991","TT":"1321","rest":"607"},"12113085":{"mass":"1500","lifetime":"100","LL":"3246","DD":"683","TT":"555","rest":"100"},"12113086":{"mass":"2000","lifetime":"100","LL":"3285","DD":"561","TT":"422","rest":"68"},"12113088":{"mass":"3000","lifetime":"100","LL":"3361","DD":"510","TT":"303","rest":"33"},"12113089":{"mass":"3500","lifetime":"100","LL":"3287","DD":"474","TT":"291","rest":"23"},"12113091":{"mass":"4500","lifetime":"100","LL":"3481","DD":"394","TT":"183","rest":"23"},"12113092":{"mass":"2500","lifetime":"1","LL":"2671","DD":"0","TT":"0","rest":"0"},"12113094":{"mass":"2500","lifetime":"1000","LL":"939","DD":"662","TT":"1456","rest":"1360"},"12113096":{"mass":"1000","lifetime":"100","LL":"2992","DD":"834","TT":"824","rest":"210"},"12113097":{"mass":"2500","lifetime":"100","LL":"3319","DD":"520","TT":"367","rest":"41"},"12113098":{"mass":"4000","lifetime":"100","LL":"3389","DD":"415","TT":"220","rest":"20"},"12113099":{"mass":"2500","lifetime":"10","LL":"3398","DD":"12","TT":"2","rest":"0"}}

#,"12113083":["800","100"],"12113085":["1500","100"],"12113086":["2000","100"],"12113088":["3000","100"],"12113089":["3500","100"],"12113091":["4500","100"],"12113092":["2500","1"],"12113094":["2500","1000"],"12113095":["250","100"],"12113096":["1000","100"],"12113097":["2500","100"],"12113098":["4000","100"],"12113099":["2500","10"]}


class lhcb_info():
    def __init__(self):

        self.subdetectors_Zpos = {1:["VELO",835.],2:["UT",2327.5],3:["SciFi",7826.],4:["M2",15000]}

        
class labels():
    def __init__(self):
        self.label_title = {"endvertexz":{"Bplus":"End vertex Z (B^{+})[mm]","Higgs0":"End vertex Z (H_{0})[mm]"},"decaylength":{"Bplus":"Decay length (B^{+})[mm]","Higgs0":"Decay length (H_{0})[mm]"},"mumuangle":"Angle(#mu^{+},#mu^{-}) [rad]","helicity":"Helicity angle (H_{0}) [rad]","helicity_cos":"Cosine of helicity angle (H_{0})","pt":{"Bplus":"P_{T} (B^{+})[MeV]","Higgs0":"P_{T} (H_{0})[MeV]","kplus":"P_{T} (K^{+})[MeV]","muplus":"P_{T} (#mu^{+})[MeV]","muminus":"P_{T} (#mu^{-})[MeV]"},"ptot":{"Bplus":"P (B^{+})[MeV]","Higgs0":"P (H_{0})[MeV]","kplus":"P (K^{+})[MeV]","muplus":"P (#mu^{+})[MeV]","muminus":"P (#mu^{-})[MeV]"}}
