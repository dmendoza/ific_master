from ROOT import *
from ROOT import TMath
from Particle_data import particles_info, lhcb_info, labels
from array import array

darkboson=particles_info().dark_boson
subdet = lhcb_info().subdetectors_Zpos
label = labels().label_title

gStyle.SetOptStat(0)
inputDir = "/afs/cern.ch/work/d/dmendoza/public/summer_project/diracSandbox/Analisis/tuples"
color   = [1,2,4,418,6]#46,7,8,5,41,11,12,16,28]#black, red, ?¿ and  blue
def plotOneVariable(name, variable, listOfTrees, listOfTreeNames, templateHist):
    #Initialisation
    C = TCanvas("C","",1200,800)
    C.SetGridx()
    C.SetGridy()

    logy = False
    if logy: C.SetLogy() 

    #Creating and setting the histograms
    myHists = [templateHist.Clone(listOfTreeNames[i]) for i, t in enumerate(listOfTrees)]
    for i, h in enumerate(myHists):
        h.Sumw2()
        listOfTrees[i].Project(listOfTreeNames[i],variable) 
        h.SetLineColor(color[i])
        h.SetMarkerColor(color[i])
        h.SetLineWidth(2)
        
        h.Scale(1/h.Integral())
#        print (i, h, listOfTreeNames[i])


#        for j in range(h.GetNbinsX()):
#         print(h.GetBinContent(j))

    maxVal = max([max([h.GetBinContent(i) for i in range(1,h.GetNbinsX()+1)]) for h in myHists])# what I want is to make sure the maximum value of all hists appears on the Canvas
    #Here I get the max value of X in the histo
    max_X = myHists[0].GetXaxis().GetXmax()

    if name == "Higgs0_TRUEENDVERTEX_Z":
        myLines = [TLine(subdet[i][1],0,subdet[i][1],maxVal) for i in range(1,5)]
        
        
        myTexts = [TPaveText(subdet[i][1]-max_X/40,maxVal+maxVal/20,subdet[i][1]+max_X/40,maxVal+maxVal/10) for i in range (1,5)] if not logy else [TPaveText(subdet[i][1]-max_X/40,maxVal+maxVal/20,subdet[i][1]+max_X/40,log(maxVal/1.2)) for i in range (1,5)]


        
        half_width = myHists[0].GetXaxis().GetBinWidth(1)/2.
        
        # channel = array('f', [ 0. ])
        # decays_1 = array('f', [ 0. ])
        # decays_2 = array('f', [ 0. ])
        # decays_3 = array('f', [ 0. ])
        # decays_4 = array('f', [ 0. ])
        # decays_1_err = array('f', [ 0. ])
        # decays_2_err = array('f', [ 0. ])
        # decays_3_err = array('f', [ 0. ])
        # decays_4_err = array('f', [ 0. ])

        # #To create the root file
        # new = True
        # if new == True:
        #     fl = TFile.Open("Z_tables.root","recreate")
        #     tree = TTree("ENDVERTEX_Z","ENDVERTEX_Z")
        #     tree.Branch("Decay_channel",channel[0])#,"Decay_channel/F")
        #     tree.Branch("Velo",decays_1[0])#,"Velo/F")
            # tree.Branch("Velo_err",decays_1_err,"Velo_err/F")
            # tree.Branch("UT",decays_2,"UT/F")
            # tree.Branch("UT_err",decays_2_err,"UT_err/F")
            # tree.Branch("Scifi",decays_3,"Scifi/F")
            # tree.Branch("Scifi_err",decays_3_err,"Scifi_err/F")
            # tree.Branch("Rest",decays_4,"M2/F")
            # tree.Branch("Rest_err",decays_4_err,"M2_err/F")

      

        

#         f_table = open("test.txt","w")


#         for i,h in enumerate(myHists):
#             decays_1 = 0
#             decays_2 = 0
#             decays_3 = 0
#             decays_4 = 0
#             #These are the square of the error of the quantity I want to represent(just counts the bincontent of each bin in the region)
#            # decays_1_err = 0
#            # decays_2_err = 0
#            # decays_3_err = 0
#            # decays_4_err = 0

#             for j in range(1,myHists[0].GetNbinsX()+1):
#                 zPos = h.GetBinCenter(j)-half_width
#                 if (zPos < subdet[1][1]): 
#                     decays_1+=h.GetBinContent(j)
#             #        decays_1_err += pow(h.GetBinError(j),2)
#                 else:
#                     if (zPos < subdet[2][1]):
#                         decays_2+=h.GetBinContent(j)
#             #            decays_2_err += pow(h.GetBinError(j),2)
#                     else:
#                         if (zPos < subdet[3][1]):
#                             decays_3+=h.GetBinContent(j)
#             #                decays_3_err += pow(h.GetBinError(j),2)
#                         else:
#                             decays_4+=h.GetBinContent(j)
#             #                decays_4_err += pow(h.GetBinError(j),2)

#             #Get the values in the overflow bin
#             decays_4 += h.GetBinContent(myHists[0].GetNbinsX()+1)
# #            decays_4_err += pow(h.GetBinError(myHists[0].GetNbinsX()+1),2)
            

            
#             channel = listOfTreeNames[i]
#             print(channel,decays_1,decays_2,decays_3,decays_4)
            
#             f_table.write(channel+" "+str(int(decays_1))+" "+str(int(decays_2))+" "+str(int(decays_3))+" "+str(int(decays_4))+" \n")
            
#         f_table.close()

#             #del tree
#             #del fl
    #Plotting
    opt = "E1"
    
    l = TLegend(0.7,0.3,0.88,0.88)

    l.SetFillColor(0)
    l.SetBorderSize(0)
    l.AddEntry("l1","LHCb simulation","")
    l.AddEntry("l2","pp #sqrt{s} = 14 TeV","")
    l.AddEntry("l3","Scalar dark boson H_{0} #rightarrow #mu^{+}#mu^{-}","")
    
    
    for i,h in enumerate(myHists):
        h.GetYaxis().SetRangeUser(0.001,maxVal+maxVal/10)
        h.Draw(opt)
        opt = "SAME E1"
        l.AddEntry(h,"M = "+darkboson[listOfTreeNames[i]]["mass"]+" MeV, #tau = "+darkboson[listOfTreeNames[i]]["lifetime"]+ " ps")
        
        # print(h.GetName())
        #Legend
    l.Draw()
    
    if name == "Higgs0_TRUEENDVERTEX_Z":
        #range 3 so it does not draw the last line (muons)
        for i in range(4):
            myLines[i].SetLineColor(kRed)
            myLines[i].Draw()
            myTexts[i].AddText(subdet[i+1][0])
            myTexts[i].Draw()

    #Saving

    C.SaveAs(name+".png")
    C.SaveAs(name+".pdf")
    #End of the function
    del C
    for h in myHists:
        del h

if True:
    #make your list of trees
    # a super nice pythonic way to do that is thus
    decTypes = ["12113092","12113099","12113097","12113094"]
#["12113092","12113099","12113097","12113094"]
#["12113082","12113085","12113097","12113089","12113091"]
#["12113082","12113083","12113085","12113086","12113088","12113089","12113091","12113092","12113094","12113096","12113097","12113098","12113099"]
    allFiles = [TFile.Open(inputDir+"/DVntuple_"+decType+".root") for decType in decTypes]
    #allTrees = []
    #for i, f in enumerate(allFiles):
    #    allTrees.append(f.Get("MCDecayTreeTuple/MCDecayTree"))
    #    allTrees[-1].SetName(decTypes[i])
    allTrees = [f.Get("MCDecayTreeTuple/MCDecayTree") for f in allFiles]
    plotOneVariable("mumu_Theta","mumu_Theta",allTrees, decTypes, TH1F("h",";"+label["mumuangle"]+";Arbitrary",11,0.,0.8))
    
    for f in allFiles: f.Close() 
