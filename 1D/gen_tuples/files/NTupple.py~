

"""Configure the variables below with:
decay: Decay you want to inspect, using 'newer' LoKi decay descriptor syntax,
decay_heads: Particles you'd like to see the decay tree of,
datafile: Where the file created by the Gauss generation phase is, and
year: What year the MC is simulating.
"""
from Gaudi.Configuration import *
from Configurables import LHCb__ParticlePropertySvc as ParticlePropertySvc
from Configurables import (
    DaVinci,
    MCDecayTreeTuple

)
from DecayTreeTuple.Configuration import *
from GaudiConf import IOHelper
from Gaudi.Configuration import appendPostConfigAction
from LoKiPhys.decorators import *
import glob

decay = "[B+ -> ^K+ ^(H_10 -> ^mu+ ^mu-) ]CC"
inputDir = "/afs/cern.ch/work/d/dmendoza/public/summer_project/diracSandbox/"

def DoIt():

    extension = "digi"
    ext = extension.upper()

    from Configurables import DataOnDemandSvc
    dod  = DataOnDemandSvc ()
    from copy import deepcopy
    algs = deepcopy ( dod.AlgMap )
    bad  = set()
    for key in algs :
        if     0 <= key.find ( 'Rec'     )                  : bad.add ( key )
        elif   0 <= key.find ( 'Raw'     )                  : bad.add ( key )
        elif   0 <= key.find ( 'DAQ'     )                  : bad.add ( key )
        elif   0 <= key.find ( 'Trigger' )                  : bad.add ( key )
        elif   0 <= key.find ( 'Phys'    )                  : bad.add ( key )
        elif   0 <= key.find ( 'Prev/'   )                  : bad.add ( key )
        elif   0 <= key.find ( 'Next/'   )                  : bad.add ( key )
        elif   0 <= key.find ( '/MC/'    ) and 'GEN' == ext : bad.add ( key )

    for b in bad :
        del algs[b]

    dod.AlgMap = algs

    from Configurables import EventClockSvc, CondDB
    EventClockSvc ( EventTimeDecoder = "FakeEventTime" )
    CondDB  ( IgnoreHeartBeat = True )



def makeOneTuple(decType):
    datafile =  inputDir+"output/dmendoza."+decType+"/*/Gauss.xgen" #this is IF the inputFiles understand "*". I am not sure it does.
    # If you want to have the list of files that match a wildcard expression (*, ?, .) you do

    listOfFiles = glob.glob(datafile) #this gives you the list of files matching: inputDir+"dmendoza."+decType+"/WHATEVER/dmendoza."+decType+"/WHATEVER/WHATEVER.xgen"
    

    ParticlePropertySvc().Particles = [ "H_10     87      25  0.0        2.50   1.0000e-9      Higgs0   25   0.000000e+000" ]
    ApplicationMgr().ExtSvc    += [ ParticlePropertySvc() ]

    # For a quick and dirty check, you don't need to edit anything below here.
    ##########################################################################

    # Create an MC DTT containing any candidates matching the decay descriptor
    mctuple = MCDecayTreeTuple("MCDecayTreeTuple")
    mctuple.Decay = decay
    mctuple.addBranches({'Bplus'  : '[B+ -> K+ (H_10 -> mu+ mu-) ]CC',
                         'Kplus'  : '[B+ -> ^K+ (H_10 -> mu+ mu-) ]CC',
                         'Higgs0' : '[B+ -> K+ ^(H_10 -> mu+ mu-) ]CC',
                         'muplus' : '[B+ -> K+ (H_10 -> ^mu+ mu-) ]CC',
                         'muminus': '[B+ -> K+ (H_10 -> mu+ ^mu-) ]CC'})

    mctuple.ToolList=[]
    #For all particles
    mctuple.addTupleTool('MCTupleToolAngles')
    mctuple.addTupleTool('MCTupleToolPID')
    mctuple.addTupleTool('MCTupleToolKinematic')
    mctuple.addTupleTool('MCTupleToolPrimaries')

    
    # Use the local input data
    IOHelper().inputFiles(listOfFiles,clear=True)

    # Configure DaVinci
    DaVinci().TupleFile = "DVntuple_"+decType+".root"
    DaVinci().Simulation = True
    DaVinci().Lumi = False
    DaVinci().PrintFreq = 1000
    DaVinci().DataType = 'Upgrade'
    DaVinci().InputType = 'DIGI'
    DaVinci().EvtMax = -1
    DaVinci().UserAlgorithms = [mctuple]

    appendPostConfigAction( DoIt )


#If you import this file, you can reuse makeOneTuple(decFile) however your wish
# But the line below only executes if you type python THISFILE.py
if True:
    makeOneTuple("500")
