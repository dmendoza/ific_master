from ROOT import *
import numpy as np
import sys
import os
from array import array
from Particle_data import labels,lhcb_info

subdet = lhcb_info().subdetectors_Zpos
label = labels().label_title
inputDir="/afs/cern.ch/work/d/dmendoza/public/IFIC/Frontiers/L0/tuplas"

#Funtion that obtains the number of track type or TT,DD,LL, or rest for a decay channel for "true" data
def track_num(treeList,massList,lifetimeList,reconstr = False):
    file_vertex = open("tracks_vertex.txt","w")
    if not reconstr: # in ["TT","LL","DD","rest"]:
        print ("not reconstructed")
        for i,tree in enumerate(treeList):
            val = array('d',[0.])
            tree.SetBranchAddress("L0_END_VZ",val)
            LL =   0
            DD =   0
            TT =   0
            rest = 0
            
            for j in range(tree.GetEntries()):
                tree.GetEntry(j)
                if (val[0] < subdet[1][1]):
                    LL+=1                                        
                else:                                                                   
                    if (val[0] < subdet[2][1]): 
                        DD+=1                                    
                    else:                                                               
                        if (val[0] < subdet[3][1]):  
                            TT+=1                                
                        else:                                                           
                            rest+=1
#            file_vertex.write('"'+decType[i]+'":{"mass":"'+massList[i]+'","lifetime":"'+lifetimeList[i]+'","LL":"'+str(int(LL))+'","DD":"'+str(int(DD))+'","TT":"'+str(int(TT))+'","rest":"'+str(int(rest))+'"},\n')                                             


    else:
        print("reconstructible")
        for i,tree in enumerate(treeList):
            LL = tree.GetEntries("proton_HASVELO==1 && pi_minus_HASVELO==1 && proton_HAST==1 && pi_minus_HAST==1 && proton_ORIGIN_VX == pi_minus_ORIGIN_VX")
            DD = tree.GetEntries("proton_HASVELO==0 && pi_minus_HASVELO==0 && proton_HAST==1 && pi_minus_HAST==1 && proton_HASTT==1 && pi_minus_HASTT==1 && proton_ORIGIN_VX == pi_minus_ORIGIN_VX")
            TT = tree.GetEntries("proton_HASTT==0 && pi_minus_HASTT==0 && proton_HAST==1 && pi_minus_HAST==1 && proton_HASVELO==0 && pi_minus_HASVELO==0 && proton_ORIGIN_VX == pi_minus_ORIGIN_VX")
            out = tree.GetEntries("proton_RECBLE==0 && pi_minus_RECBLE==0")
            notrec = tree.GetEntries("proton_RECBLE==1 && pi_minus_RECBLE==1")
            up = tree.GetEntries("proton_RECBLE==4 && pi_minus_RECBLE==4")
            tot = tree.GetEntries()
            tot_sum = LL+DD+TT+out+notrec+up
            print("tot: ",tot)
            print("tot_suma: ",tot_sum)

            file_vertex.write('"Lb":{"mass":"'+str(massList[i])+'","lifetime":"'+str(+lifetimeList[i])+'","LL":"'+str(int(LL))+'","DD":"'+str(int(DD))+'","TT":"'+str(int(TT))+'","out":"'+str(int(out))+'","not reconstructed":"'+str(int(notrec))+'","up":"'+str(int(up))+'"},')                                             

    file_vertex.close()



 
if True:
    decNumbers = []
#    lifetimes = []
#    masses = []

    lifetime = [2.631e-10]
    mass = [1115.683]
    #[1.0000e-12,1.0000e-11,1.0000e-10,2.5000e-10,5.0000e-10,7.5000e-10,1.0000e-9,1.2500e-9,1.5000e-9,1.7500e-9,2.000e-9]
    
    f = TFile.Open(inputDir+"/DV_tuple_xgen.root")
    allTrees = [f.Get("MCTuple/DecayTree")]
    
    track_num(allTrees,mass,lifetime,True)
