from ROOT import *
from ROOT import TMath

def workWithOneChain(filePattern, treeName):
    #For instance /afs/cern.ch/work/d/dmendoza/public/summer_project/diracSandbox/Analisis/2Dmap_reconstr/tuples/*root MCDecayTreeTuple/MCDecayTree
    chain = TChain(treeName)
    n = chain.Add(filePattern) # understands *, n is the number of trees found
    print("INFO: Acquired "+str(n)+" trees.")
    #chain now contains your 800 trees or so
    #chain inherits from tree, so all methods are valid Draw, Scan, GetEntries, etc.
    #Print however, will print all trees, so it s a bit long.
    print(chain.GetEntries())
    return chain

def projectFromChain(treeChain,variables,conditions,bins,xmin,xmax,histName):
    
    C = TCanvas()
    hist = TH1F("h1",histName,bins,xmin,xmax)
    treeChain.Project('h1',variables,conditions,'')
    hist.Draw()
    C.SetLeftMargin(0.15)
    C.SetRightMargin(0.15)
    C.SaveAs(histName+".png")
    del C,hist

def projectFromChain_2D(treeChain,variables,conditions,binsx,xmin,xmax,binsy,ymin,ymax,histName):
    
    C = TCanvas()
    hist = TH2F("h2","",binsx,xmin,xmax,binsy,ymin,ymax)
    treeChain.Project('h2',variables,conditions,'')
    hist.SetTitle(histName)
    hist.Draw("colz")
    
    # #line1 = TLine(2327.5,719.98,7826.,2420.)
    # line1 = TLine(-3000,2500,3000,2500.)
    # line1.SetLineColor(kRed)
    # line1.Draw("same")
    # line2 = TLine(3000.,2500.,3000.,-2500.)
    # line2.SetLineColor(kRed)
    # line2.Draw("same")
    # line3 = TLine(3000.,-2500.,-3000.,-2500.)
    # line3.SetLineColor(kRed)
    # line3.Draw("same")
    # line4 = TLine(-3000.,-2500.,-3000.,2500.)
    # line4.SetLineColor(kRed)
    # line4.Draw("same")
    C.SetLeftMargin(0.15)
    C.SetRightMargin(0.15)
    C.SaveAs(histName+".png")

    del C,hist

gStyle.SetOptStat(0)
from Particle_data import labels,lhcb_info

subdet = lhcb_info().subdetectors_Zpos
d1 = subdet[2][1]
d2 = subdet[3][1]
print(d1)
print(d2)
chain = workWithOneChain("/afs/cern.ch/work/d/dmendoza/public/summer_project/diracSandbox/Analisis/2Dmap_reconstr/tuples/*root", "MCDecayTreeTuple/MCDecayTree")

#projectFromChain(chain,'muplus_Reconstructible','Higgs0_TRUEENDVERTEX_Z>'+str(d1)+'&& Higgs0_TRUEENDVERTEX_Z<'+str(d2),7,-0.5,6.5,';VertexType;Number of events')
projectFromChain_2D(chain,'(TMath::Sin(TMath::ATan(Higgs0_TRUEPT/Higgs0_TRUEP_Z))):TMath::Sqrt(Higgs0_TRUEP_E*Higgs0_TRUEP_E-Higgs0_TRUEP_X*Higgs0_TRUEP_X-Higgs0_TRUEP_Y*Higgs0_TRUEP_Y-Higgs0_TRUEP_Z*Higgs0_TRUEP_Z)','Higgs0_TRUEENDVERTEX_Z>'+str(d1)+' && Higgs0_TRUEENDVERTEX_Z<'+str(d2),9,250.,4750.,50,0.,0.5,';Invariant mass [MeV];Sin(#theta)')

#
# projectFromChain(chain,'muplus_TRUEPT','muplus_Reconstructible==0 && Higgs0_TRUEENDVERTEX_Z>'+str(d1)+'&& Higgs0_TRUEENDVERTEX_Z<'+str(d2),20,0.,14000,'P_{T}(#mu^{+},TID==0,TT_z_pos)[MeV]')
# projectFromChain(chain,'muplus_TRUEP_Z','muplus_Reconstructible==0 && Higgs0_TRUEENDVERTEX_Z>'+str(d1)+'&& Higgs0_TRUEENDVERTEX_Z<'+str(d2),20,0.,150000,'P_{Z}(#mu^{+},TID==0,TT_z_pos)[MeV]')
# projectFromChain(chain,'TMath::Sqrt(muplus_TRUEP_Z*muplus_TRUEP_Z+muplus_TRUEPT*muplus_TRUEPT)','muplus_Reconstructible==0 && Higgs0_TRUEENDVERTEX_Z>'+str(d1)+'&& Higgs0_TRUEENDVERTEX_Z<'+str(d2),20,0.,180000,'P (#mu^{+},TID==0,TT_z_pos)[MeV]')
#projectFromChain(chain,'TMath::ATan(muplus_TRUEPT/muplus_TRUEP_Z)','muplus_Reconstructible==0 && Higgs0_TRUEENDVERTEX_Z>'+str(d1)+'&& Higgs0_TRUEENDVERTEX_Z<'+str(d2),15,0.,0.5,'Polar angle(#mu^{+},TID==0,TT_z_pos)[MeV]')
# projectFromChain_2D(chain,'muplus_TRUEORIGINVERTEX_X+(7826.1-muplus_TRUEORIGINVERTEX_Z)*muplus_TRUEPT/muplus_TRUEP_Z*TMath::Cos(TMath::ATan(muplus_TRUEP_Y/muplus_TRUEP_X)):muplus_TRUEORIGINVERTEX_Y+(7826.1-muplus_TRUEORIGINVERTEX_Z)*muplus_TRUEPT/muplus_TRUEP_Z*TMath::Sin(TMath::ATan(muplus_TRUEP_Y/muplus_TRUEP_X))','muplus_Reconstructible==0 && Higgs0_TRUEENDVERTEX_Z>'+str(d1)+'&& Higgs0_TRUEENDVERTEX_Z<'+str(d2),81,-4000.,4000.,51,-3000.,3000.,'Not reconstructed #mu^{+},first Scifi layer;X[mm];Y[mm];Tracks')
# projectFromChain_2D(chain,'muplus_TRUEORIGINVERTEX_X+(9402.9-muplus_TRUEORIGINVERTEX_Z)*muplus_TRUEPT/muplus_TRUEP_Z*TMath::Cos(TMath::ATan(muplus_TRUEP_Y/muplus_TRUEP_X)):muplus_TRUEORIGINVERTEX_Y+(9402.9-muplus_TRUEORIGINVERTEX_Z)*muplus_TRUEPT/muplus_TRUEP_Z*TMath::Sin(TMath::ATan(muplus_TRUEP_Y/muplus_TRUEP_X))','muplus_Reconstructible==0 && Higgs0_TRUEENDVERTEX_Z>'+str(d1)+'&& Higgs0_TRUEENDVERTEX_Z<'+str(d2),81,-4000.,4000.,51,-3000.,3000.,'Not reconstructed #mu^{+},last Scifi layer;X[mm];Y[mm];Tracks')
# projectFromChain_2D(chain,'muminus_TRUEORIGINVERTEX_X+(7826.1-muminus_TRUEORIGINVERTEX_Z)*muminus_TRUEPT/muminus_TRUEP_Z*TMath::Cos(TMath::ATan(muminus_TRUEP_Y/muminus_TRUEP_X)):muminus_TRUEORIGINVERTEX_Y+(7826.1-muminus_TRUEORIGINVERTEX_Z)*muminus_TRUEPT/muminus_TRUEP_Z*TMath::Sin(TMath::ATan(muminus_TRUEP_Y/muminus_TRUEP_X))','muminus_Reconstructible==0 && Higgs0_TRUEENDVERTEX_Z>'+str(d1)+'&& Higgs0_TRUEENDVERTEX_Z<'+str(d2),81,-4000.,4000.,51,-3000.,3000.,'Not reconstructed #mu^{-},first Scifi layer;X[mm];Y[mm];Tracks')
# projectFromChain_2D(chain,'muminus_TRUEORIGINVERTEX_X+(9402.9-muminus_TRUEORIGINVERTEX_Z)*muminus_TRUEPT/muminus_TRUEP_Z*TMath::Cos(TMath::ATan(muminus_TRUEP_Y/muminus_TRUEP_X)):muminus_TRUEORIGINVERTEX_Y+(9402.9-muminus_TRUEORIGINVERTEX_Z)*muminus_TRUEPT/muminus_TRUEP_Z*TMath::Sin(TMath::ATan(muminus_TRUEP_Y/muminus_TRUEP_X))','muminus_Reconstructible==0 && Higgs0_TRUEENDVERTEX_Z>'+str(d1)+'&& Higgs0_TRUEENDVERTEX_Z<'+str(d2),81,-4000.,4000.,51,-3000.,3000.,'Not reconstructed #mu^{-},last Scifi layer;X[mm];Y[mm];Tracks')
