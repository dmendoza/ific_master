#Function to obtain the sin theta (sin of mixing angle) from mass of particles, lifetime and other constants
from ROOT import *
import numpy as np
import Particle_data
from Particle_data import particles_info, lhcb_info, labels
#Constants
muMass = 105.6583745 #Mev
piMass = 139.57
kaonMass = 493.67
tauMass = 1776.99
darkboson = particles_info().scalarDark_boson_reconstr_hack

h_bar = 6.58211928e-22 #units: Mev * sec
Gf = 1.1663787e-11 #Mev to -2

def computeSintheta(mh2,tau):
    
    if mh2 > 2*tauMass:
        return (2./tauMass)*pow(1.-4.*pow(tauMass/mh2,2),-3./8.)*TMath.Sqrt(TMath.Sqrt(2.)*np.pi*h_bar/(tau*Gf*mh2))+(2./muMass)*pow(1.-4.*pow(muMass/mh2,2),-3./8.)*TMath.Sqrt(TMath.Sqrt(2.)*np.pi*h_bar/(tau*Gf*mh2))
    else:
        return (2./muMass)*pow(1.-4.*pow(muMass/mh2,2),-3./8.)*TMath.Sqrt(TMath.Sqrt(2.)*np.pi*h_bar/(tau*Gf*mh2))

def create2Dgraph(lifetimes,masses,trackType):

    numPoints = len(lifetimes)*len(masses)
    grph2d = TGraph2D(numPoints)
    n = 0
    for i in range(len(masses)):
        for j in range(len(lifetimes)):
            decnum = masses[i]+j
            grph2d.SetPoint(n,masses[i],computeSintheta(masses[i],lifetimes[j]),(float(darkboson[str(decnum)][trackType]))/(float(darkboson[str(decnum)]["LL"])+float(darkboson[str(decnum)]["DD"])+float(darkboson[str(decnum)]["TT"])+float(darkboson[str(decnum)]["rest"])))
            n+=1
#computeSintheta(masses[i],lifetimes[j])
#float(darkboson[str(decnum)]["TT"])
    return grph2d

def create1Dgraph(lifetime,masses):

    numPoints = len(masses)
    grph1d = TGraph(numPoints)
    n = 0
    for i in range(len(masses)):
        grph1d.SetPoint(n,masses[i],computeSintheta(masses[i],lifetime))
        n+=1
    return grph1d

if True:

    lifetime = [1.0000e-12,1.0000e-11,1.0000e-10,2.5000e-10,5.0000e-10,7.5000e-10,1.0000e-9,1.2500e-9,1.5000e-9,1.7500e-9,2.000e-9]
    mass = [500,1000,1500,2000,2500,3000,3500,4000,4500]
    
    C = TCanvas()
    C.SetRightMargin(0.20)
    C.SetBottomMargin(0.13)
    C.SetLeftMargin(0.15)
    
    

    gr2d = create2Dgraph(lifetime,mass,"DD")
    gr2d.SetTitle(';M (H_{0}) [MeV];Sin(#Theta);DD vertex proportion')
    gr2d.SetNpx(500)
    gr2d.SetNpy(500)

#    gStyle.SetLabelSize(0.04,"xyz")
    

    gr2d.Draw("colz")    
    h = gr2d.GetHistogram()

    h.GetYaxis().CenterTitle()
    h.GetXaxis().CenterTitle()
    h.GetZaxis().CenterTitle()


    h.GetYaxis().SetTitleSize(0.045)
    h.GetXaxis().SetTitleSize(0.045)
    h.GetZaxis().SetTitleSize(0.045)

    h.GetYaxis().SetLabelSize(0.04)
    h.GetXaxis().SetLabelSize(0.04)
    h.GetZaxis().SetLabelSize(0.04)

    
    h.GetXaxis().SetLabelOffset(0.005)
    h.GetYaxis().SetLabelOffset(0.005)
    h.GetZaxis().SetLabelOffset(0.005)
    
    h.GetXaxis().SetTitleOffset(1.3)
    h.GetYaxis().SetTitleOffset(1.3)
    h.GetZaxis().SetTitleOffset(1.3)
    
    h.GetXaxis().SetLabelFont(132)
    h.GetYaxis().SetLabelFont(132)
    h.GetZaxis().SetLabelFont(132)
    
    h.GetXaxis().SetTitleFont(132)
    h.GetYaxis().SetTitleFont(132)
    h.GetZaxis().SetTitleFont(132)
    h.GetZaxis().SetRangeUser(0.,0.75)
    
    grphs1d = []
    l = 0
    for j in range(len(lifetime)):
        if lifetime[j] in [1.0000e-12,1.0000e-11,1.0000e-10,5.0000e-10,1.0000e-9,2.000e-9]:
            grphs1d.append(create1Dgraph(lifetime[j],mass))
            grphs1d[l].SetMarkerColor(kRed)
            grphs1d[l].SetLineStyle(9)
            grphs1d[l].SetLineWidth(2)
            latex = TLatex(grphs1d[l].GetX()[5], grphs1d[l].GetY()[5],str(int(lifetime[j]*1.e12))+" ps")
            latex.SetTextSize(0.038)
            latex.SetTextFont(132)
            grphs1d[l].GetListOfFunctions().Add(latex)
            grphs1d[l].Draw("SAME C")
            l+=1
            
    

    l = TLegend(0.55,0.77,0.75,0.89)
    l.SetHeader("LHCb simulation","L")
#    l.AddEntry("l1","LHCb simulation","")
    l.AddEntry(grphs1d[0],"H_{0} lifetime","l")
    l.SetFillStyle(0)
    l.SetBorderSize(0)
    l.SetTextFont(132)
    l.SetTextSize(0.05)
    l.Draw()

    
#    l.AddEntry("l2","pp #sqrt{s} = 14 TeV","")
#    l.AddEntry("l3","Scalar dark boson H_{0} #rightarrow #mu^{+}#mu^{-}","")


    C.SetLogy()
#    C.SetLogz()
    C.SaveAs("DDTT_mixAngle.png")
    C.SaveAs("DDTT_mixAngle.pdf")

    del C,grphs1d,gr2d

    

